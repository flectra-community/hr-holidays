# Copyright (C) 2018 Brainbean Apps (https://brainbeanapps.com)
# Copyright 2020 CorporateHub (https://corporatehub.eu)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    "name": "Leave Credit",
    "version": "2.0.1.0.0",
    "category": "Human Resources",
    "website": "https://gitlab.com/flectra-community/hr-holidays",
    "author": "CorporateHub, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "installable": True,
    "application": False,
    "summary": "Enable negative leave balance for employees",
    "depends": ["hr_holidays"],
    "data": ["views/hr_leave_type.xml"],
}
