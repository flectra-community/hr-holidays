# Flectra Community / hr-holidays

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[hr_holidays_credit](hr_holidays_credit/) | 2.0.1.0.0| Enable negative leave balance for employees
[hr_holidays_public](hr_holidays_public/) | 2.0.2.0.5| Manage Public Holidays


